set nocompatible
set encoding=utf-8
set backspace=indent,eol,start

syntax enable
set background=dark
set t_Co=256

colorscheme solarized

filetype on

call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()

let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='solarized'
let g:airline_solarized_bg='dark'
let g:airline_powerline_fonts = 1

let g:coc_disable_startup_warning = 1
let g:coc_global_extensions = [
	\'coc-sh',
	\'coc-clangd',
	\'coc-json',
	\'coc-marketplace',
	\'coc-prettier',
	\'coc-python',
	\'coc-vimlsp',
	\'coc-snippets',
	\'coc-explorer',
	\'coc-cmake',
        \'coc-tsserver',
	\'coc-react-refactor',
	\'coc-eslint',
	\'coc-styled-components',
	\'coc-go']

hi Pmenu ctermbg=Black ctermfg=White
hi PmenuSel ctermbg=White ctermfg=Black

inoremap <silent><expr> <TAB>
	\ coc#pum#visible() ? coc#pum#next(1) :
	\ CheckBackspace() ? "\<TAB>" :
	\ coc#refresh()
function! CheckBackspace()
	let col = col('.') - 1
	return !col || getline('.')[col - 1] =~# '\s'
endfunction

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

set cmdheight=2
set updatetime=300
set shortmess+=c


if has("patch-8.1.1564")
	set signcolumn=number
else
	set signcolumn=yes
endif


if has('nvim')
	inoremap <silent><expr> <c-space> coc#refresh()
else
	inoremap <silent><expr> <c-@> coc#refresh()
endif

nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	elseif (coc#rpc#ready())
		call CocActionAsync('doHover')
	else
		execute '!' . &keywordprg . " " . expand('<cword>')
	endif
endfunction

autocmd CursorHold * silent call CocActionAsync('highlight')

noremap <C-Tab> :<C-U>tabnext<CR>
inoremap <C-Tab> <C-\><C-N>:tabnext<CR>
cnoremap <C-Tab> <C-C>:tabnext<CR>

noremap <C-S-Tab> :<C-U>tabprevious<CR>
inoremap <C-S-Tab> <C-\><C-N>:tabprevious<CR>
cnoremap <C-S-Tab> <C-C>:tabprevious<CR>

function SetupPython()
	set tabstop=4
	set shiftwidth=4
	set softtabstop=4
	set expandtab
	set smarttab
	set colorcolumn=80
endfunction

function SetupYAML()
	set tabstop=4
	set shiftwidth=4
	set softtabstop=4
	set expandtab
	set smarttab
endfunction

function SetupJSON()
	set tabstop=4
	set shiftwidth=4
	set softtabstop=4 
	set expandtab
	set smarttab
endfunction()

function SetupHTML()
	set tabstop=2
	set shiftwidth=2
	set softtabstop=2
	set expandtab
	set smarttab
endfunction()

function SetupCSS()
	set tabstop=2
	set shiftwidth=2
	set softtabstop=2
	set expandtab
	set smarttab
endfunction()

function SetupJS()
	set tabstop=2
	set shiftwidth=2
	set softtabstop=2
	set expandtab
	set smarttab
endfunction()

function ClangFormatBuffer()
	if !empty(findfile('.clang-format', expand('%:p:h') . ';'))
		let cursor_pos = getpos('.')
		:%!clang-format
		call setpos('.', cursor_pos)
	endif
endfunction

function SetupCXX()
	set tabstop=8
	set shiftwidth=8
	set softtabstop=8
	set noexpandtab
	set smarttab
	set colorcolumn=101
	nnoremap <C-f> :call ClangFormatBuffer()<CR>
endfunction()

function! GoFormatBuffer()
	if !executable('gofmt')
		echoerr "gofmt not found. Please install gofmt."
		return
	endif

	let saved_view = winsaveview()
	silent %!gofmt
	if v:shell_error > 0
		cexpr getline(1, '$')->map({ idx, val -> val->substitute('<standard input>', expand('%'), '') })
		silent undo
		cwindow
	endif
	call winrestview(saved_view)
endfunction


function SetupGo()
	set tabstop=8
	set shiftwidth=8
	set softtabstop=8
	set noexpandtab
	set smarttab
	set colorcolumn=101
	nnoremap <C-f> :call GoFormatBuffer()<CR>
endfunction()

set tabstop=8
set shiftwidth=8
set softtabstop=8
set noexpandtab
set smarttab
set colorcolumn=101

au BufNew,BufRead *.html :call SetupHTML()
au BufNew,BufRead *.css,*.scss :call SetupCSS()
au BufNew,BufRead *.js,*.jsx,*.ts,*.tsx :call SetupJS()
au BufNew,BufRead *.py :call SetupPython()
au BufNew,BufRead *.c,*.h,*.hh,*.cc,*.hpp,*.cpp,*.hxx,*.cxx,*.h++,*.c++,*.m,*.mm :call SetupCXX()
au BufNew,BufRead *.yaml,*.yml :call SetupYAML()
au BufNew,BufRead *.raml :call SetupYAML()
au BufNew,BufRead *.json :call SetupJSON()
au BufNew,BufRead *.go :call SetupGo()

hi CocFloating ctermbg=black

