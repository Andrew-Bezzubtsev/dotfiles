# Dotfiles
This repository consists of a sequence of RC files and profiles for
variety of console applications, such as Vim, ZSH, etc...

In the foreseeable future, there will appear a script for installation of
custom rc, config files and plugins.

## Avalible configuration files

### Xorg
Xorg configuration files are located at:
* .Xresources.d/
* .Xresources

The Xorg configuration defines some settings (mostly, coloring) for 
rxvt-unicode terminal.

### Awesome WM
Awesome WM configuration gives you some desktop environment, which I liked
some time before, but not now. Now I am using a different desktop environment
& window manager due to lack of integration between software, I could
implement by-hand.

### tmux
The tmux configuration sets handy hotkeys for tmux default commands and
configures a pretty one status bar. Moreover, it enables mouse support.

### zsh
zsh configuration provides a pretty prompt with some git information about
current repository on the right side.
Further, the configuration gives some aliases out-of-box for cmake & qmake
tools, selects clang and clang++ as default compilers.

### clang-format 
clang-format configuration is vim-centered a bit. There is autoformatting
implemented in my vim configuration. clang-format utility is used
to perform this autoformatting. The chosen format is a mix of LLVM and
Kernigan-Ritchie coding style.

### Vim
Vim configuration includes autocompletion, coloring and pretty vim airline theme. Moreover, it implements autoformatting your files on-save or toggling Ctrl-f combination.

## Installation

First of all, install: vim, tmux, zsh, xclip, clang, boost, libclang. Configure your locale properly!

    git clone https://github.com/Andrew-Bezzubtsev/dotfiles.git
    git clone https://github.com/powerline/fonts.git
    cd fonts
    ./install.sh
    cd ..
    cd dotfiles
    ./merge.py install
    cd ..
    chsh -s /bin/zsh
    cd ~/.vim
    mkdir bundle
    cd bundle
    git clone https://github.com/VundleVim/Vundle.vim
    vim +PluginInstall
    cd YouCompleteMe
    ./install.py --system-libclang --system-boost --clang-completer

Now select some powerline-patched font for your terminal emulator and reboot
to apply shell selection. It's worth selecting solarized dark theme for your
terminal emulator!
