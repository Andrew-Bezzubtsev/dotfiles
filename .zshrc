export PATH=$PATH

autoload -U colors && colors
autoload -U compinit && compinit
autoload -U promptinit && promptinit

[[ -a $(whence -p pacman-color) ]] && compdef _pacman pacman-color=pacman

zstyle ':completion:*' menu select=long-list select=0
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

for f in $HOME/.zshrc.d/**/*; do
	if [[ -r $f ]]; then
		source $f
	fi
done

if [[ -d "/opt/bin" ]]; then
	export PATH="$PATH:/opt/bin"
fi

if [[ -d "/usr/local/bin" ]]; then
	export PATH="/usr/local/bin:$PATH"
fi

if [[ -d "/Library/TeX/texbin" ]]; then
	export PATH="$PATH:/Library/TeX/texbin"
fi

if [[ -d "/usr/local/go/bin" ]]; then
	export PATH="$PATH:/usr/local/go/bin"
fi

if [[ -d "$HOME/go/bin" ]]; then
	export PATH="$PATH:$HOME/go/bin"
fi

if [[ -z "$TMUX" ]]; then
	ID=`tmux ls | grep -vm1 attached | cut -d: -f1`

	while [[ -n "$ID" ]]; do
		tmux kill-session -t "$ID"
		ID=`tmux ls | grep -vm1 attached | cut -d: -f1`
	done

	exec tmux
fi
